#include "application.h"
#include "sdl2.h"
#include <stdio.h>
#include "wall.h"
#include "game.h"
#include "character.h"

struct Application *Application_initialize() {
    struct Application* application;
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        fprintf(stderr, "SDL failed to initialize: %s\n", SDL_GetError());
        return NULL;
    }
    if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
        fprintf(stderr, "Warning: Linear texture filtering not enabled!");
    }
    application = (struct Application*)malloc(sizeof(struct Application) + NUMWALL * sizeof(struct Wall));
    application->window = SDL_CreateWindow(WINDOW_TITLE,
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (application->window == NULL) {
        fprintf(stderr, "Window could not be created: %s\n", SDL_GetError());
        return NULL;
    }
    application->renderer = SDL_CreateRenderer(application->window, -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (application->renderer == NULL) {
        fprintf(stderr, "Renderer could not be created: %s\n", SDL_GetError());
        return NULL;
    }
    SDL_SetRenderDrawColor(application->renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(application->renderer);
    SDL_RenderPresent(application->renderer);
    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) {
        fprintf(stderr, "SDL_Image failed to initialize: %s\n", IMG_GetError());
        return NULL;
    }
    application->game = Game_initialize(application->renderer);
    if (application->game == NULL) {
        fprintf(stderr, "Failed to initialize game: %s\n", IMG_GetError());
        application->state = APPLICATION_STATE_FINISHED;
    }
    application->gameWon = GameWon_initialize(application->renderer);
    if (application->gameWon == NULL) {
        fprintf(stderr, "Failed to initialize gameWon: %s\n", IMG_GetError());
        application->state = APPLICATION_STATE_FINISHED;
    }
    application->menu = Menu_initialize(application->renderer);
    if (application->menu == NULL) {
        fprintf(stderr, "Failed to initialize menu: %s\n", IMG_GetError());
	return NULL;
    }
    application->credits = Credits_initialize(application->renderer);
    if (application->credits == NULL) {
        fprintf(stderr, "Failed to initialize credits: %s\n", IMG_GetError()); 
    }
    application->options = Options_initialize(application->renderer);
    if (application->options == NULL) {
	fprintf(stderr, "Failed to initialize options: %s\n", IMG_GetError()); 
    }
    application->options->optionsDonuts = OptionsDonuts_initialize(application->renderer);
    if (application->options->optionsDonuts == NULL) {
        fprintf(stderr, "Failed to initialize optionsDonuts: %s\n", IMG_GetError()); 
    }	
    application->options->optionsWall = OptionsWall_initialize(application->renderer);
    if (application->options->optionsWall == NULL) {
        fprintf(stderr, "Failed to initialize optionsWall: %s\n", IMG_GetError()); 
    }
    application->state = APPLICATION_STATE_MAIN_MENU;
    return application;
}

void Application_run(struct Application *application) {
    SDL_Event e;
    while (application->state != APPLICATION_STATE_FINISHED) {
        switch (application->state) {
            case APPLICATION_STATE_MAIN_MENU:
                Menu_run(application->menu);
                if (application->menu->option == MENU_QUIT) {
	    	    application->state = APPLICATION_STATE_FINISHED;
	    	} else if (application->menu->option == MENU_PLAY) {
	            application->state = APPLICATION_STATE_PLAYING;
		} else if (application->menu->option == MENU_OPTIONS) {
		    application->state = APPLICATION_STATE_OPTIONS_MENU;
		} else if (application->menu->option == MENU_CREDITS) {
		    application->state = APPLICATION_STATE_CREDITS_SCREEN;
		}
		break;
	    case APPLICATION_STATE_CREDITS_SCREEN:
		Credits_run(application->credits, application->menu);
		if (application->menu->option == MENU_QUIT) {
	            application->state = APPLICATION_STATE_FINISHED;	
		} else {
		    application->state = APPLICATION_STATE_MAIN_MENU;
		}
		break;
            case APPLICATION_STATE_OPTIONS_MENU:
		Options_run(application->options, application->menu);

		if (application->menu->option == MENU_QUIT) {
		    application->state = APPLICATION_STATE_FINISHED;	
		} else {
		    application->state = APPLICATION_STATE_MAIN_MENU;
		}
		break;
	    case APPLICATION_STATE_PLAYING:
                Game_options(application->options, application->game);
                Game_run(application->game);
                if (application->game->state == GAME_QUIT) {
                    application->state = APPLICATION_STATE_FINISHED;
                } else if (application->game->state == GAME_WON) {
                    application->state = APPLICATION_STATE_GAME_WON;
                }
                break;
            case APPLICATION_STATE_GAME_WON:
                GameWon_run(application->gameWon);
                Character_delete(application->game->character);
                application->game->character = Character_create(application->game->renderer);
                application->state = APPLICATION_STATE_MAIN_MENU;
                break;
            case APPLICATION_STATE_FINISHED:
                break;
        }
    }
}

void Application_shutDown(struct Application* application) {
    SDL_DestroyRenderer(application->renderer);
    SDL_DestroyWindow(application->window);
    Game_delete(application->game);
    GameWon_delete(application->gameWon);
    Menu_delete(application->menu);
    Credits_delete(application->credits);
    Options_delete(application->options);
    free(application);
    application = NULL;
    IMG_Quit();
    SDL_Quit();
}
