#ifndef OPTIONS_H
#define OPTIONS_H

#include "sdl2.h"
#include "constants.h"
#include "menu.h"

enum OptionsOptions {
    OPTIONS_NONE,
    OPTIONS_BACK
};

enum NumDonuts {
    FIVE_DONUTS,
    TEN_DONUTS,
    FIFTEEN_DONUTS,
};

enum WallColor {
    RED_WALL,
    BLUE_WALL,
    GREEN_WALL,
};

struct Options {
    enum OptionsOptions option;
    struct OptionsDonuts *optionsDonuts;
    struct OptionsWall *optionsWall;
    struct Spritesheet *options;
    struct Spritesheet *wallR;
    struct Spritesheet *wallB;
    struct Spritesheet *wallG;
    struct Spritesheet *donuts5;
    struct Spritesheet *donuts10;
    struct Spritesheet *donuts15;
    struct Spritesheet *back;
    SDL_Renderer *renderer;
};

struct OptionsDonuts {
    enum OptionsOptions option;
    enum NumDonuts numDonuts;
    struct Spritesheet *optionsDonuts;
    SDL_Renderer *renderer;
};

struct OptionsWall {
    enum OptionsOptions option;
    enum WallColor wallColor;
    struct Spritesheet *optionsWall;
    SDL_Renderer *renderer;
};

struct Options* Options_initialize(SDL_Renderer *renderer);
struct OptionsDonuts* OptionsDonuts_initialize(SDL_Renderer *renderer);
struct OptionsWall* OptionsWall_initialize(SDL_Renderer *renderer);

void Options_run(struct Options *options, struct Menu *menu);
void OptionsDonuts_run(struct OptionsDonuts *options, struct Menu *menu);
void OptionsWall_run(struct OptionsWall *options, struct Menu *menu);

void Options_delete(struct Options *options);
void OptionsWall_delete(struct OptionsWall * optionsWall);
void OptionsDonuts_delete(struct OptionsDonuts * optionsDonuts);
#endif
