#ifndef DONUT_H
#define DONUT_H

#include "sdl2.h"
#include "utils.h"
#include "animated_spritesheet.h"
#include "boxCollider.h"

struct Donut {
    struct AnimatedSpritesheet *animatedspritesheet;
    struct BoxCollider collider;
    struct Point position;
    bool collided;
    SDL_Renderer *renderer;
};

struct Donut * Donut_create(SDL_Renderer * renderer, double posX, double posY, const char *filename); 

void Donut_render(struct Donut *donut);

void Donut_delete(struct Donut *donut);

#endif
