#ifndef DONUT_C
#define DONUT_C

#include "donut.h"
#include "constants.h"
#include "sdl2.h"

struct Donut *Donut_create(SDL_Renderer * renderer, double posX, double posY, const char *filename) {
    struct Donut *donut;
    donut = (struct Donut *) malloc(sizeof(struct Donut) + sizeof(SDL_Rect));
    donut->renderer = renderer;
    donut->position.x = posX;
    donut->position.y = posY;
    donut->collided = false;
    donut->animatedspritesheet = AnimatedSpritesheet_create(DONUT_FILENAME,1,32,32,20,renderer);
    donut->animatedspritesheet->spritesheet->scale = 0.25;
    donut->collider.collider.x = posX; 
    donut->collider.collider.y = posY;
    donut->collider.collider.h =  (donut->animatedspritesheet->spritesheet->spriteHeight) * 0.25;
    donut->collider.collider.w =  (donut->animatedspritesheet->spritesheet->spriteWidth) * 0.25;
    SDL_RenderDrawRect(renderer,&donut->collider.collider);
    return donut;
}

void Donut_delete(struct Donut *donut) {
    if (donut != NULL) {
        AnimatedSpritesheet_delete(donut->animatedspritesheet);
        free(donut);
    }
}

void Donut_render(struct Donut *donut) {
    if (!donut->collided) {
    int elapsed;
    elapsed = SDL_GetTicks() - donut->animatedspritesheet->lastUpdate;
        if (elapsed > donut->animatedspritesheet->delayBetweenFrame) {
            int f = elapsed / donut->animatedspritesheet->delayBetweenFrame;
            donut->animatedspritesheet->currentColumn =
	    (donut->animatedspritesheet->currentColumn + f) %
	    donut->animatedspritesheet->spritesheet->numColumns;
            donut->animatedspritesheet->lastUpdate += elapsed;
        }
        int currentFrame = donut->animatedspritesheet->currentRow *
		   donut->animatedspritesheet->spritesheet->numColumns                    + donut->animatedspritesheet->currentColumn;
        Spritesheet_render(donut->animatedspritesheet->spritesheet,
        donut->position.x, donut->position.y, currentFrame);
    }
}


#endif
