#include "game.h"
#include "sdl2.h"
#include "constants.h"
#include "spritesheet.h"
#include "boxCollider.h"
#include "character.h"
#include "wall.h"
#include "utils.h"
#include "donut.h"
#include "animated_spritesheet.h"
#include "options.h"

// --------------------------- //
// Private function prototypes //
// --------------------------- //
//bool Game_validMove(struct Game *game, enum Direction direction);
const int LIST_WALL_POSITION_X[NUMWALL] = {0,0,0,875,112,550,-200,200,425,675,675};
const int LIST_WALL_POSITION_Y[NUMWALL] = {0,575,0,0,475,-125,175,375,275,175,375};
const int LIST_DONUT_POSITION_X[15] = {460,50,50,825,50,825,825,825,825,825,50,480,300,585,585};
const int LIST_DONUT_POSITION_Y[15] = {425,50,525,525,100,50,100,225,275,325,225,50,325,50,225};
const char * LIST_WALL_RED_FILENAME[NUMWALL] = { LONG_WALL_RED_HOR1_FILENAME,
                                             LONG_WALL_RED_HOR1_FILENAME,
                                             LONG_WALL_RED_VER_FILENAME,
                                             LONG_WALL_RED_VER_FILENAME,
                                             LONG_WALL_RED_HOR2_FILENAME,
                                             LONG_WALL_RED_VER_FILENAME,
                                             LONG_WALL_RED_HOR2_FILENAME,
                                             LONG_WALL_RED_HOR3_FILENAME,
                                             LONG_WALL_RED_HOR3_FILENAME,
                                             LONG_WALL_RED_HOR3_FILENAME,
                                             LONG_WALL_RED_HOR3_FILENAME};
const char * LIST_WALL_BLUE_FILENAME[NUMWALL] = { LONG_WALL_BLUE_HOR1_FILENAME,
                                             LONG_WALL_BLUE_HOR1_FILENAME,
                                             LONG_WALL_BLUE_VER_FILENAME,
                                             LONG_WALL_BLUE_VER_FILENAME,
                                             LONG_WALL_BLUE_HOR2_FILENAME,
                                             LONG_WALL_BLUE_VER_FILENAME,
                                             LONG_WALL_BLUE_HOR2_FILENAME,
                                             LONG_WALL_BLUE_HOR3_FILENAME,
                                             LONG_WALL_BLUE_HOR3_FILENAME,
                                             LONG_WALL_BLUE_HOR3_FILENAME,
                                             LONG_WALL_BLUE_HOR3_FILENAME};
const char * LIST_WALL_GREEN_FILENAME[NUMWALL] = { LONG_WALL_GREEN_HOR1_FILENAME,
                                             LONG_WALL_GREEN_HOR1_FILENAME,
                                             LONG_WALL_GREEN_VER_FILENAME,
                                             LONG_WALL_GREEN_VER_FILENAME,
                                             LONG_WALL_GREEN_HOR2_FILENAME,
                                             LONG_WALL_GREEN_VER_FILENAME,
                                             LONG_WALL_GREEN_HOR2_FILENAME,
                                             LONG_WALL_GREEN_HOR3_FILENAME,
                                             LONG_WALL_GREEN_HOR3_FILENAME,
                                             LONG_WALL_GREEN_HOR3_FILENAME,
                                             LONG_WALL_GREEN_HOR3_FILENAME};


struct Game *Game_initialize(SDL_Renderer *renderer) {
    struct Game *game;
    game = (struct Game*)malloc(sizeof(struct Game) + NUMWALL * sizeof(struct Wall));
    game->wall = (struct Wall**)malloc(NUMWALL * sizeof(struct Wall));
    game->renderer = renderer;
    game->character = Character_create(renderer);
    unsigned int i;
    for (i = 0; i < NUMWALL; i++) {
        game->wall[i] = Wall_create(renderer, LIST_WALL_POSITION_X[i],
                        LIST_WALL_POSITION_Y[i], LIST_WALL_RED_FILENAME[i]);
    }
    game->numDonuts = 5;
    game->donut = (struct Donut**)malloc(game->numDonuts * sizeof(struct Donut));
    game->donutsCollected = 0;
    for (i = 0; i < game->numDonuts; i++) {
        game->donut[i] = Donut_create(game->renderer, LIST_DONUT_POSITION_X[i],LIST_DONUT_POSITION_Y[i], DONUT_FILENAME);
    }

    game->state = GAME_PLAY;
    return game;
}

struct GameWon *GameWon_initialize(SDL_Renderer *renderer) {
    struct GameWon *gameWon;
    gameWon = (struct GameWon*)malloc(sizeof(struct GameWon));
    gameWon->renderer = renderer;
    gameWon->option = OPTION_NONE;
    gameWon->gameWon = Spritesheet_create(GAME_WON_FILENAME, 1, 1, 1, renderer);
    return gameWon;
}

void Game_options(struct Options *options, struct Game *game) {
    unsigned int i;
    switch(options->optionsWall->wallColor) {
        case RED_WALL:
            for (i = 0; i < NUMWALL; i++) {
                Wall_delete(game->wall[i]);
                game->wall[i] = Wall_create(game->renderer, LIST_WALL_POSITION_X[i],
                        LIST_WALL_POSITION_Y[i], LIST_WALL_RED_FILENAME[i]);
            }
            break;
        case BLUE_WALL:
            for (i = 0; i < NUMWALL; i++) {
                Wall_delete(game->wall[i]);
                game->wall[i] = Wall_create(game->renderer, LIST_WALL_POSITION_X[i],
                        LIST_WALL_POSITION_Y[i], LIST_WALL_BLUE_FILENAME[i]);
            }
            break;
        case GREEN_WALL:
            for (i = 0; i < NUMWALL; i++) {
                Wall_delete(game->wall[i]);
                game->wall[i] = Wall_create(game->renderer, LIST_WALL_POSITION_X[i],
                        LIST_WALL_POSITION_Y[i], LIST_WALL_GREEN_FILENAME[i]);
            }
            break;
    }
    if (game->donut != NULL) {
        unsigned int i = 0;
        for (i = 0; i < game->numDonuts; i++) {
            Donut_delete(game->donut[i]);
        }
        free(game->donut);
    }
    switch(options->optionsDonuts->numDonuts) {
        case FIVE_DONUTS:
            game->numDonuts = 5;
            break;
        case TEN_DONUTS:
            game->numDonuts = 10;
            break;
        case FIFTEEN_DONUTS:
            game->numDonuts = 15;
            break;
    }
    game->donut = (struct Donut**)malloc(game->numDonuts * sizeof(struct Donut));
    for (i = 0; i < game->numDonuts; i++) {
        game->donut[i] = Donut_create(game->renderer, LIST_DONUT_POSITION_X[i],LIST_DONUT_POSITION_Y[i], DONUT_FILENAME);
    }
}

void Game_delete(struct Game *game) {
    if (game != NULL) {
        Character_delete(game->character);
        if (game->wall != NULL) {
            unsigned int j = 0;
            for (j = 0; j < NUMWALL; j++) {
                Wall_delete(game->wall[j]);
            }
            free(game->wall);
        }
        if (game->donut != NULL) {
            unsigned int i = 0;
            for (i = 0; i < game->numDonuts; i++) {
                Donut_delete(game->donut[i]);
            }
            free(game->donut);
        }
        free(game);
    }
}

void GameWon_delete(struct GameWon *gameWon) {
    if (gameWon != NULL) {
        Spritesheet_delete(gameWon->gameWon);
        free(gameWon);
    }
}

void GameWon_run(struct GameWon *gameWon) {
    SDL_Event e;
    gameWon->option = OPTION_NONE;
    while (gameWon->option == OPTION_NONE) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                gameWon->option = OPTION_BACK;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        gameWon->option = OPTION_BACK;
                        break;
                }
            }
        }
        SDL_SetRenderDrawColor(gameWon->renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(gameWon->renderer);
        Spritesheet_render(gameWon->gameWon, 0, 0, 0);
        SDL_RenderPresent(gameWon->renderer);
    }
}

void Game_run(struct Game *game) {
    SDL_Event e;
    game->donutsCollected = 0;
    game->state = GAME_PLAY;
    
    while (game->state == GAME_PLAY) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                game->state = GAME_QUIT;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_LEFT:
                        if (game->character->velocity.x != -1) {
                            game->character->velocity.x = -1;
                            if (game->character->touchGround)
                                Character_changeState(game->character, BOB_WALKING_LEFT);
                            else
                                Character_changeState(game->character, BOB_JUMPING_LEFT);
                        }
                        break;
                    case SDLK_RIGHT:
                        if (game->character->velocity.x != 1) {
                            game->character->velocity.x = 1;
                            if (game->character->touchGround)
                                Character_changeState(game->character, BOB_WALKING_RIGHT);
                            else
                                Character_changeState(game->character, BOB_JUMPING_RIGHT);
                        }
                        break;
                    case SDLK_UP:
                        if (game->character->touchGround) 
                            Character_jump(game->character);
                        break;
                }
            } else if (e.type == SDL_KEYUP) {
                switch (e.key.keysym.sym) {
                    case SDLK_LEFT:
                        game->character->velocity.x = 0;
                        break;
                    case SDLK_RIGHT:
                        game->character->velocity.x = 0;
                        break;
                }
            }
        }
        
        // Collision
        bool wallUnderneath = false;
        unsigned int i = 0;
        for (i = 0; i < NUMWALL; i++) {
            check_collision(&game->character->collider,&game->wall[i]->collider);
            // Collision on right side
            if (game->character->collider.colDir.right && game->character->velocity.x == 1) 
                game->character->velocity.x = 0;
            // Collision on left side
            if (game->character->collider.colDir.left && game->character->velocity.x == -1) 
                game->character->velocity.x = 0;
            // Collision on top side
            if (game->character->collider.colDir.top && game->character->velocity.y == -1) 
                game->character->jumping = false;
            // Collision on bottom side
            if (game->character->collider.colDir.bottom && game->character->velocity.y == 1) {
                game->character->touchGround = true;
                if (game->character->velocity.x == 0)
                    Character_changeState(game->character, BOB_IDLE);
                else if (game->character->velocity.x == -1)
                    Character_changeState(game->character, BOB_WALKING_LEFT);
                else if (game->character->velocity.x == 1)
                    Character_changeState(game->character, BOB_WALKING_RIGHT);
            }

            // If there is a bottom collision with one of the walls
            if (game->character->collider.colDir.bottom)
                wallUnderneath = true;
        }
        
        if (!wallUnderneath)
            game->character->touchGround = false;

	    unsigned j;
        for (j = 0; j < game->numDonuts; j++) {	
            check_collision(&game->character->collider, &game->donut[j]->collider);
            if (game->character->collider.colDir.right ||
                game->character->collider.colDir.left ||
                game->character->collider.colDir.top ||
                game->character->collider.colDir.bottom) {
                game->donut[j]->collided = true;
            }
        }

        for (j = 0; j < game->numDonuts; j++) {
            if (game->donut[j]->collided == true) {
                game->donutsCollected += 1;
            }
        }
        if (game->donutsCollected >= game->numDonuts) {
            game->state = GAME_WON;                    
        } else {
            game->donutsCollected = 0;
        }

        SDL_SetRenderDrawColor(game->renderer, 0xFF, 0xFF, 0xFF, 0xFF );
        SDL_RenderClear(game->renderer);
        unsigned int k;
        for (k = 0; k < NUMWALL; k++) {
            Wall_render(game->wall[k]);
        }

        unsigned int l;
        for (l = 0; l < game->numDonuts; l++) {
            Donut_render(game->donut[l]);
        }
        Character_move(game->character);
        Character_render(game->character);

        SDL_RenderPresent(game->renderer);
    }
}


