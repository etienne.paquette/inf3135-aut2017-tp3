#ifndef CREDITS_H
#define CREDITS_H

#include "sdl2.h"
#include "constants.h"
#include "menu.h"

enum CreditsOptions {
    CREDITS_NONE,
    CREDITS_BACK
};

struct Credits {
    enum CreditsOptions option;
    struct Spritesheet *credits;
    SDL_Renderer *renderer;
};

struct Credits* Credits_initialize(SDL_Renderer *renderer);

void Credits_run(struct Credits *credits, struct Menu *menu);

void Credits_delete(struct Credits *credits);

#endif
