#ifndef WALL_H
#define WALL_H

#include "sdl2.h"
#include "utils.h"
#include "spritesheet.h"
#include "boxCollider.h"

struct Wall {
    struct Spritesheet *spritesheet;
    struct BoxCollider collider;
    struct Point position;
    SDL_Renderer *renderer;
};

struct Wall * Wall_create(SDL_Renderer * renderer, double posX, double posY, const char *filename); 

void Wall_delete(struct Wall *wall);

void Wall_render(struct Wall *wall);

#endif
