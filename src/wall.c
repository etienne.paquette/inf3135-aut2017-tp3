#ifndef WALL_C
#define WALL_C

#include "wall.h"
#include "constants.h"
#include "sdl2.h"

struct Wall * Wall_create(SDL_Renderer * renderer, double posX, double posY, const char *filename) {
    struct Wall * wall;
    wall = (struct Wall *) malloc(sizeof(struct Wall) + 1000 + sizeof(SDL_Rect));
    wall->renderer = renderer;
    wall->position.x = posX;
    wall->position.y = posY;
    wall->spritesheet = Spritesheet_create(filename,1,1,1,renderer);
    wall->collider.collider.x = posX; 
    wall->collider.collider.y = posY;
    wall->collider.collider.h =  (wall -> spritesheet -> spriteHeight);
    wall->collider.collider.w =  (wall -> spritesheet -> spriteWidth);
    return wall;
}

void Wall_delete(struct Wall *wall) {
    if (wall != NULL) {
        Spritesheet_delete(wall->spritesheet);
        free(wall);
    }
}

void Wall_render(struct Wall *wall) {
    Spritesheet_render(wall->spritesheet,wall->position.x, wall->position.y,0);
}

#endif
