#ifndef BOXCOLLIDER_C
#define BOXCOLLIDER_C

#include "boxCollider.h"
#include <stdbool.h>

void check_collision (struct BoxCollider* boxA, struct BoxCollider* boxB) {
    double leftA,leftB;
    double rightA,rightB;
    double topA,topB;
    double bottomA,bottomB;

    leftA = boxA->collider.x;
    rightA = boxA->collider.x + boxA->collider.w;
    topA = boxA->collider.y;
    bottomA = boxA->collider.y + boxA->collider.h;

    leftB = boxB->collider.x;
    rightB = boxB->collider.x + boxB->collider.w;
    topB = boxB->collider.y;
    bottomB = boxB->collider.y + boxB->collider.h;
   
    boxA -> colDir.bottom = false;       
    boxA -> colDir.top = false;
    boxA -> colDir.right = false;
    boxA -> colDir.left = false;
    if( bottomA > topB && bottomA < bottomB  && rightA > leftB+5 && leftA < rightB -5 ) {
        boxA -> colDir.bottom = true;
    }
    if( topA < bottomB && topA > topB  && rightA > leftB+5 && leftA < rightB-5 ) {
        boxA -> colDir.top = true;
    }
    if( rightA > leftB && rightA < rightB  && topA < bottomB-5 && bottomA > topB+5 ) {
        boxA -> colDir.right = true;
    }
    if( leftA < rightB && leftA > leftB  && topA < bottomB-5 && bottomA > topB+5 ) {
        boxA -> colDir.left = true;
    }

}
    

#endif
