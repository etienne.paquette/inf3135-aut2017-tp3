#ifndef SPRITESHEET_H
#define SPRITESHEET_H

#include "sdl2.h"

struct Spritesheet {
	int numRows;
	int numColumns;
	int numSprites;
	int spriteWidth;
	int spriteHeight;
	float scale;
	SDL_Texture *texture;
	SDL_Renderer *renderer;
};

struct Spritesheet *Spritesheet_create (const char *filename, int numRows, int numColumns,
										int numSprites, SDL_Renderer *renderer);

void Spritesheet_render (struct Spritesheet *spritesheet, int x, int y, int frame);

void Spritesheet_delete (struct Spritesheet *spritesheet);

#endif
