#ifndef CHARACTER_H
#define CHARACTER_H

#include <stdbool.h>
#include "sdl2.h"
#include "utils.h"
#include "animated_spritesheet.h"
#include "boxCollider.h"

// --------------- //
// Data structures //
// --------------- //

enum BobState {
    BOB_IDLE,
    BOB_WALKING_RIGHT,
    BOB_WALKING_LEFT,
    BOB_JUMPING_VERTICALLY,
    BOB_JUMPING_RIGHT,
    BOB_JUMPING_LEFT
};

struct Character {
    struct AnimatedSpritesheet *animatedspritesheet;
    enum BobState state;
    struct BoxCollider collider;
    int frame;
    struct Point position;
    struct Point velocity;
    bool touchGround;
    bool jumping;
    float jumpStart, jumpEnd;
    bool moving;
    SDL_Renderer *renderer;
};

// --------- //
// Functions //
// --------- //

/**
 * Creates the character.
 *
 * @param renderer   The renderer
 * @return           A pointer to the character, NULL if there was an error;
 *                   Call IMG_GetError() for more information.
 */
struct Character *Character_create(SDL_Renderer *renderer);

/**
 * Deletes the character.
 *
 * @param character  The character to delete
 */
void Character_delete(struct Character *character);

/**
 * Renders the character.
 *
 * @param character  The character to render
 */
void Character_render(struct Character *character);

/**
 * Moves the character.
 *
 * @param character  The character to move
 */
void Character_move(struct Character *character);

/**
 * Start the character jump
 *
 * @param character  The character to jump
 */
void Character_jump(struct Character *character);

/**
 * Change the character state
 *
 * @param character  The character to change
 * @param state      The state in which we want to change
 */
void Character_changeState(struct Character *character, enum BobState state);

#endif
