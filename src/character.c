#ifndef CHARACTER_C
#define CHARACTER_C

#include "constants.h"
#include "boxCollider.h"
#include "character.h"
#include "sdl2.h"
#include "animated_spritesheet.h"

struct Character *Character_create(SDL_Renderer *renderer) {
    struct Character *character;
    character = (struct Character*)malloc(sizeof(struct Character) + sizeof(SDL_Rect));
    character->renderer = renderer;
    character->position.x = START_X;
    character->position.y = START_Y;
    character->collider.collider.x = START_X + 15;
    character->collider.collider.y = START_Y;
    character->moving = false;
    character->animatedspritesheet = AnimatedSpritesheet_create(CHARACTER_SPRITESHEET, 6, 32, 192, CHARACTER_BETWEEN_FRAME, renderer);
    character->animatedspritesheet->spritesheet->scale = 0.5;
    character->collider.collider.h = character->animatedspritesheet->spritesheet->spriteHeight/2;
    character->collider.collider.w = character->animatedspritesheet->spritesheet->spriteWidth/4;
    character->velocity.x = 0;
    character->velocity.y = 1;
    character->touchGround = false; 
    character->jumping = false;
    character->animatedspritesheet->running = true;
    Character_changeState(character, BOB_IDLE);
    return character;
}

void Character_delete(struct Character *character) {
    if (character != NULL) {
        AnimatedSpritesheet_delete(character->animatedspritesheet);
        free(character);
    }
}

void Character_render(struct Character *character) {
    AnimatedSpritesheet_render(character->animatedspritesheet, character->position.x, character->position.y);
}

void Character_move(struct Character *character) {
    if (character->velocity.x == 0 && character->velocity.y == 0) {
        Character_changeState(character, BOB_IDLE);
        return;
   }

    if (character->jumping && SDL_GetTicks() > character->jumpEnd)
        character->jumping = false;

    if (character->touchGround)
        character->velocity.y = 0;

    if (!character->touchGround && !character->jumping) 
        character->velocity.y = 1;
    
    character->position.x += character->velocity.x * CHARACTER_MOVE_SPEED;
    character->position.y += character->velocity.y * CHARACTER_MOVE_SPEED;
    character->collider.collider.x += character->velocity.x * CHARACTER_MOVE_SPEED;
    character->collider.collider.y += character->velocity.y * CHARACTER_MOVE_SPEED;
}

void Character_jump(struct Character *character) {
    int now = SDL_GetTicks();
    character->touchGround = false;
    character->jumping = true;
    character->jumpStart = now;
    character->jumpEnd = now + CHARACTER_JUMP_DURATION;
    character->velocity.y = -1;

    if (character->state == BOB_IDLE)
        Character_changeState(character, BOB_JUMPING_VERTICALLY);
    else if (character->state == BOB_WALKING_LEFT)
        Character_changeState(character, BOB_JUMPING_LEFT);
    else if (character->state == BOB_WALKING_RIGHT)
        Character_changeState(character, BOB_JUMPING_RIGHT);
}

void Character_changeState(struct Character *character, enum BobState state) {
    character->state = state;
    switch (state) {
        case BOB_IDLE:
            AnimatedSpritesheet_setRow(character->animatedspritesheet, CHARACTER_IDLE_ROW);
            break;
        case BOB_WALKING_RIGHT:
            AnimatedSpritesheet_setRow(character->animatedspritesheet, CHARACTER_WALKING_RIGHT_ROW);
            break;
        case BOB_WALKING_LEFT:
            AnimatedSpritesheet_setRow(character->animatedspritesheet, CHARACTER_WALKING_LEFT_ROW);
            break;
        case BOB_JUMPING_RIGHT:
            AnimatedSpritesheet_setRow(character->animatedspritesheet, CHARACTER_JUMPING_RIGHT_ROW);
            break;
        case BOB_JUMPING_LEFT:
            AnimatedSpritesheet_setRow(character->animatedspritesheet, CHARACTER_JUMPING_LEFT_ROW);
            break;
        case BOB_JUMPING_VERTICALLY:
            AnimatedSpritesheet_setRow(character->animatedspritesheet, CHARACTER_JUMPING_VERTICALLY_ROW);
            break;
    }
}

#endif
