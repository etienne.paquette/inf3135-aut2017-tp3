#include "options.h"
#include "sdl2.h"
#include "spritesheet.h"
#include "game.h"

struct Options *Options_initialize(SDL_Renderer *renderer) {
    struct Options *options;
    options = (struct Options*)malloc(sizeof(struct Options));
    options->renderer = renderer;
    options->option = OPTIONS_NONE;
    options->options = Spritesheet_create(OPTIONS_TITLE_FILENAME, 1, 1, 1, renderer);
    options->wallR = Spritesheet_create(OPTIONS_REDWALL_FILENAME, 1, 1, 1, renderer);
    options->wallB = Spritesheet_create(OPTIONS_BLUEWALL_FILENAME, 1, 1, 1, renderer);
    options->wallG = Spritesheet_create(OPTIONS_GREENWALL_FILENAME, 1, 1, 1, renderer);
    options->donuts5 = Spritesheet_create(OPTIONS_5DONUTS_FILENAME, 1, 1, 1, renderer);
    options->donuts10 = Spritesheet_create(OPTIONS_10DONUTS_FILENAME, 1, 1, 1, renderer);	
    options->donuts15 = Spritesheet_create(OPTIONS_15DONUTS_FILENAME, 1, 1, 1, renderer);	
    options->back = Spritesheet_create(OPTIONS_BACK_FILENAME, 1, 1, 1, renderer);
    return options;
}

struct OptionsDonuts *OptionsDonuts_initialize(SDL_Renderer *renderer) {
    struct OptionsDonuts *optionsDonuts;
    optionsDonuts = (struct OptionsDonuts*)malloc(sizeof(struct OptionsDonuts));
    optionsDonuts->renderer = renderer;
    optionsDonuts->numDonuts = FIVE_DONUTS;
    optionsDonuts->option = OPTIONS_NONE;
    optionsDonuts->optionsDonuts = Spritesheet_create(DONUTS_OPTIONS_FILENAME, 1, 1, 1, renderer);
    return optionsDonuts;
}

struct OptionsWall *OptionsWall_initialize(SDL_Renderer *renderer) {
    struct OptionsWall *optionsWall;
    optionsWall = (struct OptionsWall*)malloc(sizeof(struct OptionsWall));
    optionsWall->renderer = renderer;
    optionsWall->wallColor = BLUE_WALL;
    optionsWall->option = OPTIONS_NONE;
    optionsWall->optionsWall = Spritesheet_create(WALL_OPTIONS_FILENAME, 1, 1, 1, renderer);
    return optionsWall;
}

void Options_run(struct Options *options, struct Menu *menu) {
    SDL_Event e;
    options->option = OPTIONS_NONE;	
    while (options->option == OPTIONS_NONE) {
        if (menu->option == MENU_QUIT) {
            options->option = OPTIONS_BACK;
        }
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                menu->option = MENU_QUIT;
                options->option = OPTIONS_BACK;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        options->option = OPTIONS_BACK;
                        break;
                    case SDLK_1:	
                        OptionsDonuts_run(options->optionsDonuts, menu);
                        break;
                    case SDLK_2:
                        OptionsWall_run(options->optionsWall, menu);
                        break;
                }
            }
        }
        SDL_SetRenderDrawColor(options->renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(options->renderer);
        Spritesheet_render(options->options, OPTIONS_TITLE_X, OPTIONS_TITLE_Y, 0);
        
        if (options->optionsDonuts->numDonuts == FIFTEEN_DONUTS) {
            Spritesheet_render(options->donuts15, OPTIONS_DONUTS_X, OPTIONS_DONUTS_Y, 0);
        } else if (options->optionsDonuts->numDonuts == TEN_DONUTS) {
            Spritesheet_render(options->donuts10, OPTIONS_DONUTS_X, OPTIONS_DONUTS_Y, 0);
        } else {
            Spritesheet_render(options->donuts5, OPTIONS_DONUTS_X, OPTIONS_DONUTS_Y, 0);
        }
        
        if (options->optionsWall->wallColor == GREEN_WALL) {
            Spritesheet_render(options->wallG, OPTIONS_WALL_X, OPTIONS_WALL_Y, 0);
        } else if (options->optionsWall->wallColor == BLUE_WALL) {
            Spritesheet_render(options->wallB, OPTIONS_WALL_X, OPTIONS_WALL_Y, 0);
        } else {
            Spritesheet_render(options->wallR, OPTIONS_WALL_X, OPTIONS_WALL_Y, 0);
        }	
        Spritesheet_render(options->back, OPTIONS_BACK_X, OPTIONS_BACK_Y, 0);
        SDL_RenderPresent(options->renderer);
    }
}

void OptionsDonuts_run(struct OptionsDonuts *options, struct Menu *menu) {
    SDL_Event e;
    options->option = OPTIONS_NONE;
    while (options->option == OPTIONS_NONE) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                menu->option = MENU_QUIT;
                options->option = OPTIONS_BACK;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        options->option = OPTIONS_BACK;
                        break;
                    case SDLK_1:	
                        options->option = OPTIONS_BACK;
                        options->numDonuts = FIVE_DONUTS;
                        break;
                    case SDLK_2:
                        options->option = OPTIONS_BACK;
                        options->numDonuts = TEN_DONUTS;
                        break;
                    case SDLK_3:
                        options->option = OPTIONS_BACK;
                        options->numDonuts = FIFTEEN_DONUTS;
                }       
            }
        }
        SDL_SetRenderDrawColor(options->renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(options->renderer);
        Spritesheet_render(options->optionsDonuts, DONUTS_OPTIONS_X, DONUTS_OPTIONS_Y, 0);
        SDL_RenderPresent(options->renderer);
    }
}

void OptionsWall_run(struct OptionsWall *options, struct Menu *menu) {
    SDL_Event e;
    options->option = OPTIONS_NONE;
    while (options->option == OPTIONS_NONE) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                menu->option = MENU_QUIT;
                options->option = OPTIONS_BACK;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        options->option = OPTIONS_BACK;
                        break;
                    case SDLK_1:	
                        options->option = OPTIONS_BACK;
                        options->wallColor = RED_WALL;
                        break;
                    case SDLK_2:
                        options->option = OPTIONS_BACK;
                        options->wallColor = BLUE_WALL;
                        break;
                    case SDLK_3:
                        options->option = OPTIONS_BACK;
                        options->wallColor = GREEN_WALL;
                }
            }
        }
        SDL_SetRenderDrawColor(options->renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(options->renderer);
        Spritesheet_render(options->optionsWall, WALL_OPTIONS_X, WALL_OPTIONS_Y, 0);
        SDL_RenderPresent(options->renderer);
    }
}

void Options_delete(struct Options *options) {
    if (options != NULL) {	
        OptionsDonuts_delete(options->optionsDonuts);
        OptionsWall_delete(options->optionsWall);
        Spritesheet_delete(options->options);
        Spritesheet_delete(options->wallR);
        Spritesheet_delete(options->wallB);
        Spritesheet_delete(options->wallG);
        Spritesheet_delete(options->donuts5);
        Spritesheet_delete(options->donuts10);
        Spritesheet_delete(options->donuts15);
        Spritesheet_delete(options->back);
        free(options);
    }
}

void OptionsWall_delete (struct OptionsWall * optionsWall) {
    if (optionsWall != NULL) {
        Spritesheet_delete(optionsWall->optionsWall);
        free(optionsWall);
    }
}

void OptionsDonuts_delete (struct OptionsDonuts * optionsDonuts) {
    if (optionsDonuts != NULL) {
        Spritesheet_delete(optionsDonuts->optionsDonuts);
        free(optionsDonuts);
    }
}
