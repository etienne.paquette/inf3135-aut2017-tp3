#include "credits.h"
#include "sdl2.h"
#include "spritesheet.h"
#include "menu.h"

struct Credits *Credits_initialize(SDL_Renderer *renderer) {
    struct Credits *credits;
    credits = (struct Credits*)malloc(sizeof(struct Credits));
    credits->renderer = renderer;
    credits->option = CREDITS_NONE;
    credits->credits = Spritesheet_create(CREDITS_SCREEN_FILENAME, 1, 1, 1, renderer);
    return credits;
}

void Credits_run(struct Credits *credits, struct Menu *menu) {
    SDL_Event e;
    credits->option = CREDITS_NONE;
    while (credits->option == CREDITS_NONE) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {	
                menu->option = MENU_QUIT;
                credits->option = CREDITS_BACK;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        credits->option = CREDITS_BACK;
                    break;
                }
            }
        }
        SDL_SetRenderDrawColor(credits->renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(credits->renderer);
        Spritesheet_render(credits->credits, BG_X, BG_Y, 0);
        SDL_RenderPresent(credits->renderer);
    }
}

void Credits_delete(struct Credits *credits) {
    if (credits != NULL) {	
        Spritesheet_delete(credits->credits);
        free(credits);
    }
}
