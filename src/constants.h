// General constants
#define WINDOW_TITLE "Bob's Donuts"
#define SCREEN_WIDTH 900
#define SCREEN_HEIGHT 600

// Menu constants
#define TITLE_FILENAME "../assets/title.png"
#define TITLE_WIDTH 700
#define TITLE_X ((SCREEN_WIDTH - TITLE_WIDTH) / 2)
#define TITLE_Y 50
#define PLAY_FILENAME "../assets/play.png"
#define PLAY_WIDTH 420
#define PLAY_X ((SCREEN_WIDTH - PLAY_WIDTH) / 2)
#define PLAY_Y 200
#define OPTIONS_FILENAME "../assets/options.png"
#define OPTIONS_WIDTH 420
#define OPTIONS_X ((SCREEN_WIDTH - QUIT_WIDTH) / 2)
#define OPTIONS_Y 300
#define CREDITS_FILENAME "../assets/credits.png"
#define CREDITS_WIDTH 420
#define CREDITS_X ((SCREEN_WIDTH - QUIT_WIDTH) / 2)
#define CREDITS_Y 400
#define QUIT_FILENAME "../assets/quit.png"
#define QUIT_WIDTH 420
#define QUIT_X ((SCREEN_WIDTH - QUIT_WIDTH) / 2)
#define QUIT_Y 500
#define BG_FILENAME "../assets/bg.png"
#define BG_WIDTH 900
#define BG_X 0
#define BG_Y 0

// Credits screen constants
#define CREDITS_SCREEN_FILENAME "../assets/creditsScreen.png"
#define CREDITS_SCREEN_WIDTH 900
#define CREDITS_SCREEN_X 0
#define CREDITS_SCREEN_Y 0

// Options screen constants
#define OPTIONS_TITLE_FILENAME "../assets/optionsScreen.png"
#define OPTIONS_TITLE_WIDTH 320 
#define OPTIONS_TITLE_X ((SCREEN_WIDTH - OPTIONS_TITLE_WIDTH) / 2)
#define OPTIONS_TITLE_Y TITLE_Y 
#define OPTIONS_5DONUTS_FILENAME "../assets/currentOptions5.png"
#define OPTIONS_10DONUTS_FILENAME "../assets/currentOptions10.png"
#define OPTIONS_15DONUTS_FILENAME "../assets/currentOptions15.png"
#define OPTIONS_DONUTS_WIDTH 700
#define OPTIONS_DONUTS_X ((SCREEN_WIDTH - OPTIONS_DONUTS_WIDTH) / 2)
#define OPTIONS_DONUTS_Y 150
#define OPTIONS_REDWALL_FILENAME "../assets/currentOptionsR.png"
#define OPTIONS_BLUEWALL_FILENAME "../assets/currentOptionsB.png"
#define OPTIONS_GREENWALL_FILENAME "../assets/currentOptionsG.png"
#define OPTIONS_WALL_WIDTH 800
#define OPTIONS_WALL_X ((SCREEN_WIDTH - OPTIONS_WALL_WIDTH) / 2)
#define OPTIONS_WALL_Y 300
#define OPTIONS_BACK_FILENAME "../assets/back.png"
#define OPTIONS_BACK_WIDTH 300 
#define OPTIONS_BACK_X ((SCREEN_WIDTH - OPTIONS_BACK_WIDTH) / 2)
#define OPTIONS_BACK_Y 500

// Game constants
#define START_X 450
#define START_Y 500

// Character constants
#define CHARACTER_SPRITESHEET "../assets/character.png"
#define CHARACTER_MOVE_SPEED 4
#define CHARACTER_JUMP_DURATION 600
#define CHARACTER_BETWEEN_FRAME 20
#define CHARACTER_WALKING_RIGHT_ROW 0
#define CHARACTER_WALKING_LEFT_ROW 1
#define CHARACTER_JUMPING_RIGHT_ROW 4
#define CHARACTER_JUMPING_LEFT_ROW 5
#define CHARACTER_JUMPING_VERTICALLY_ROW 3
#define CHARACTER_IDLE_ROW 2

// Wall Options constants
#define WALL_OPTIONS_FILENAME "../assets/optionsColor.png"
#define WALL_OPTIONS_WIDTH 900
#define WALL_OPTIONS_X 0
#define WALL_OPTIONS_Y 0

// Donuts Options constants
#define DONUTS_OPTIONS_FILENAME "../assets/optionsDonuts.png"
#define DONUTS_OPTIONS_WIDTH 900
#define DONUTS_OPTIONS_X 0
#define DONUTS_OPTIONS_Y 0

// Wall constants
#define LONG_WALL_RED_HOR1_FILENAME "../assets/wall-RED-Hor1.png"
#define LONG_WALL_RED_HOR2_FILENAME "../assets/wall-RED-Hor2.png"
#define LONG_WALL_RED_HOR3_FILENAME "../assets/wall-RED-Hor3.png"
#define LONG_WALL_RED_VER_FILENAME "../assets/wall-RED-Ver.png"
#define LONG_WALL_BLUE_HOR1_FILENAME "../assets/wall-BLUE-Hor1.png"
#define LONG_WALL_BLUE_HOR2_FILENAME "../assets/wall-BLUE-Hor2.png"
#define LONG_WALL_BLUE_HOR3_FILENAME "../assets/wall-BLUE-Hor3.png"
#define LONG_WALL_BLUE_VER_FILENAME "../assets/wall-BLUE-Ver.png"
#define LONG_WALL_GREEN_HOR1_FILENAME "../assets/wall-GREEN-Hor1.png"
#define LONG_WALL_GREEN_HOR2_FILENAME "../assets/wall-GREEN-Hor2.png"
#define LONG_WALL_GREEN_HOR3_FILENAME "../assets/wall-GREEN-Hor3.png"
#define LONG_WALL_GREEN_VER_FILENAME "../assets/wall-GREEN-Ver.png"
#define NUMWALL 11 

// Donut constants
#define DONUT_FILENAME "../assets/donut.png"

// Game Won constants
#define GAME_WON_FILENAME "../assets/WinnerWinnerDonutDinner.png"
