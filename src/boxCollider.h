#ifndef BOXCOLLIDER_H
#define BOXCOLLIDER_H

#include "utils.h"
#include <stdbool.h>
#include "sdl2.h"

struct collision {
    bool right;
    bool left;
    bool top;
    bool bottom;
};

struct BoxCollider {
    SDL_Rect collider;
    struct collision colDir;
};

void check_collision (struct BoxCollider* boxA, struct BoxCollider* boxB);
    

#endif
