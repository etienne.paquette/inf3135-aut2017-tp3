#ifndef UTILS_H
#define UTILS_H

// -------------- //
// Data structure //
// -------------- //

enum Direction {
    DIRECTION_RIGHT,
    DIRECTION_LEFT,
    DIRECTION_UP,
    DIRECTION_DOWN
};  

struct Point {
    double x;
    double y;
};

#endif
