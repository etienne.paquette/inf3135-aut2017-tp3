#ifndef APPLICATION_H
#define APPLICATION_H

#include "sdl2.h"
#include "constants.h"
#include "menu.h"
#include "game.h"
#include "credits.h"
#include "options.h"

// --------------- //
// Data structures //
// --------------- //

enum ApplicationState {
    APPLICATION_STATE_MAIN_MENU,
    APPLICATION_STATE_PLAYING,
    APPLICATION_STATE_OPTIONS_MENU,
    APPLICATION_STATE_CREDITS_SCREEN,
    APPLICATION_STATE_GAME_WON,
    APPLICATION_STATE_FINISHED
};

struct Application {
    enum ApplicationState state;
    struct Game *game;
    struct GameWon *gameWon;
    struct Menu *menu;
    struct Credits *credits;
    struct Options *options;
    SDL_Window *window;
    SDL_Renderer* renderer;
};

// --------- //
// Functions //
// --------- //

struct Application* Application_initialize();

void Application_run(struct Application* application);

void Application_shutDown(struct Application* application);

#endif
