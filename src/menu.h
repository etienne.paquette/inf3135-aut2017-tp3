#ifndef MENU_H
#define MENU_H

#include "sdl2.h"
#include "constants.h"

enum MenuOptions {
    MENU_NONE,
    MENU_PLAY,
    MENU_OPTIONS,
    MENU_CREDITS,
    MENU_QUIT
};

struct Menu {
    enum MenuOptions option;
    struct Spritesheet *title;
    struct Spritesheet *play;
    struct Spritesheet *options;
    struct Spritesheet *credits;
    struct Spritesheet *quit;
    SDL_Renderer *renderer;
};

struct Menu* Menu_initialize(SDL_Renderer *renderer);

void Menu_run(struct Menu *menu);

void Menu_delete(struct Menu *menu);

#endif
