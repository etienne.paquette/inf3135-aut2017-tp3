#ifndef GAME_H
#define GAME_H

#include "sdl2.h"
#include "options.h"

// --------------- //
// Data structures //
// --------------- //

enum GameState {
    GAME_PLAY, // The player is playing
    GAME_MENU, // The player is returning to the menu
    GAME_WON,
    GAME_QUIT  // The player is quitting
};

enum GameOptions {
    OPTION_NONE,
    OPTION_BACK
};

struct Game {
    struct Character *character;
    struct Wall **wall;
    struct Donut **donut;
    int numDonuts;
    int donutsCollected;
    SDL_Renderer *renderer;      // The renderer
    enum GameState state;        // The state of the game
};

struct GameWon {
    SDL_Renderer *renderer;
    enum GameOptions option;
    struct Spritesheet *gameWon;
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a new game.
 *
 * @param   The renderer for the game
 * @return  A pointer to the game, NULL if there was an error
 */
struct Game *Game_initialize(SDL_Renderer *renderer);

struct GameWon *GameWon_initialize(SDL_Renderer *renderer);

/**
 * Delete the game.
 *
 * @param game  The game to delete
 */
void Game_delete(struct Game *game);
void GameWon_delete(struct GameWon * gameWon);
/**
 * Start running the game.
 *
 * @param game  The game to run
 */
void Game_options(struct Options *options, struct Game *game);
void Game_run(struct Game *game);
void GameWon_run(struct GameWon *gameWon);
#endif

