# Travail pratique 3

## Description

Ce programme permet de génerer un jeu video de plateformes dont
le but pour gagner la partie est que Bob mange tous les beignes.

Ce projet a été réalisé dans le cadre du cours INF3135: Construction
et maintenance de logiciels, enseigné par Alexandre Blondin Massé à la 
session d'automne 2017 à l'Université du Québec a Montréal.

## Auteurs

- Étienne Paquette Perrier (PAQE090979005)
- Audrey-Ann Raymond (RAYA28619001)
- Anthony Berleur (BERA11059408)

## Fonctionnement

Le jeu commence par un menu, comme l'affiche l'image ci-dessous:

![](assets/menuReadme.png)

pour jouer il suffit de tapper au clavier la touche demandé (dans
ce cas-ci le ``1``).
Pour voir les options, on tappe le ``2``.
Pour voir les credits, on tappe le ``3``.
Puis pour quitter on tappe ``escape``.

Le but du jeu est de manger tous les beignes dans le labyrinthe.
Lorsque le but est atteint, une fenêtre affichant le succès de la
partie apparait comme suit:

![](assets/WinnerWinnerDonutDinner.png)

Les touches permettant de faire déplacer le personnage sont:
- la flèche vers la gauche pour déplacer le personne vers la gauche,
- la flèche vers la droite pour déplacer le personnage vers la droite,
- la flèche vers le haut pour faire sauter le personnage vers le haut,
- la flèche vers la gauche et la flèche vers le haut pour faire sauter
- le personnage vers la gauche,
la flèche vers la droite et la flèche vers le haut pour faire sauter
- le personnage vers la droite.

Il y a deux options possibles, soit changer la couleur des murs du labyrinthe
ou changer le nombre de beignes à manger. 

![](assets/optionsReadme.png)

Il y a trois choix possibles pour chaque option.
 
Dans le cas des couleurs pour les murs du labyrinthe, les trois choix sont:
- Rouge,
- Bleu,
- Vert.

![](assets/optionsColor.png)

Dans le cas du nombre de beigne a manger, les trois choix sont:
- 5 beignes,
- 10 beignes,
- 15 beignes.

![](assets/optionsDonuts.png)

Les crédits montrent qui sont les auteurs du programme et qui sont
les personnes qui ont contribué au projet.

![](assets/creditsScreen.png)

## Plateformes supportées

Testé sur Ubuntu 16.04 et sur Xubuntu 18.04.

## Dépendances

[SDL2](https://www.libsdl.org/download-2.0.php), une bibliothèque fournissant un accès de bas niveau aux périphériques
audio, souris, clavier, joystick et graphique grâce à OpenGL et Direct3D.  

[SDL2-image](https://www.libsdl.org/projects/SDL_image/), une bibliothèque de chargement d'image utilisée avec la bibliothèque SDL.

## Compilation

Pour compiler, il suffit d'aller dans le répertoire ``src`` et
d'entrer la commande

``make``

puis de lancer la commande

``./tp3``

pour exécuter le programme. Bien entendu, il est nécessaire d'installer
les dépendances pour être en mesure de compiler le programme.
 
## Références

Nous avons pris comme référence le jeu Maze d'Alexandre Blondin Massé.

## Division des tâches

- [X] Affichage de la fenêtre               (Etienne)
- [X] Évênement clavier                     (Etienne)
- [X] Ajouter et retirer des Sprites        (Audrey)
- [X] Animation du personnage               (Etienne)
- [X] Animation du beigne                   (Audrey)
- [X] Collision                             (Anthony)
- [X] Menu                                  (Audrey)
- [X] Mouvement                             (Etienne)
- [X] Collection de beigne                  (Audrey)
- [X] Saut                                  (Etienne)
- [X] Design niveau                         (Anthony)
- [X] État du personnage                    (Etienne)
- [X] État du programme                     (Audrey)
- [X] Rédiger le Readme                     (Audrey)
- [X] S'assurer d'aucune fuite de mémoire   (Anthony)

## Statut

Le programme fonctionne, bien qu'il n'y a qu'un seul niveau. 
Il est impossiblede perdre une partie.
